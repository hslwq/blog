---
title: Git基础使用
date: 2021-04-22 09:48:16
index_img: https://z3.ax1x.com/2021/04/26/gpkSBR.png
tags:
- git
categories:
- [git]
---
# Git基础使用
> 本文参考 [Git教程 | 菜鸟教程](https://www.runoob.com/git/git-tutorial.html)

Git的常用命令：
- git clone
- git pull
- git add
- git commit
- git push

常用命令及不同状态之间的关系如图  
![](https://z3.ax1x.com/2021/04/26/gpPOaV.png)

说明：
- workspace：工作区
- staging area：暂存区/缓冲区
- local repository：本地仓库
- remote repository：远程仓库

## 创建仓库命令
| 命令 | 说明 |
| --- | --- |
| `git init` | 初始化/创建本地仓库 |
| `git clone` | 从远程仓库克隆一个项目 |

## 更新(同步代码)
git获取更新有两种方式

命令 | 说明
--- | ---
[`git pull`](https://www.runoob.com/git/git-pull.html) | 从远程仓库获取更新到本地仓库，并直接合并到工作区
[`git fetch`](https://www.runoob.com/git/git-fetch.html) | 从远程仓库获取更新到本地仓库


[`git merge`](http://git-scm.com/book/zh/v2/Git-%E5%88%86%E6%94%AF-%E5%88%86%E6%94%AF%E7%9A%84%E6%96%B0%E5%BB%BA%E4%B8%8E%E5%90%88%E5%B9%B6) | 合并分支

[`git log`](http://git-scm.com/book/zh/v2/Git-%E5%9F%BA%E7%A1%80-%E6%9F%A5%E7%9C%8B%E6%8F%90%E4%BA%A4%E5%8E%86%E5%8F%B2) | 查看提交历史

## 提交
命令 | 说明
--- | ---
[`git add`](https://www.runoob.com/git/git-add.html) | 添加文件到暂存区
[`git commit`](https://www.runoob.com/git/git-commit.html) | 将暂存区中的文件提交到本地仓库
[`git push`](https://www.runoob.com/git/git-push.html) | 将本地仓库代码上传到远程仓库，并且合并

## 日志


## 分支
