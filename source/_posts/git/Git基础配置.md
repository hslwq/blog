---
title: Git基础配置
date: 2021-04-22 09:15:25
index_img: https://z3.ax1x.com/2021/04/26/gpkSBR.png
tags:
- git
categories:
- [git]
---
# Git基础配置

## 生成并配置秘钥

### 设置用户名 和 登录邮箱
该步骤仅需执行一次，如果已经设置过，请跳过该步骤
```shell
git config --global user.name '用户名'

git config --global user.email '登录邮箱'
```

### 生成密钥
生成密钥前，先打开当前用户目录下的.ssh目录，查看是否存在`id_rsa.pub`文件，如果文件已经存在，则说明密钥已经生成过

密钥只需生成一次，如果已经存在，请跳过该步骤
```shell
ssh-keygen -t rsa -C '登录邮箱'
```
***上述代码执行完毕后，会连续多次要求输入密码。注意：此时不要输入密码，直接回车

### 配置
1. 打开当前用户目录下的.ssh目录，打开目录下的`id_rsa.pub`文件，内容全部复制
  ![](https://z3.ax1x.com/2021/04/22/cqhaQO.png)

2. 然后进入GitHub或Gitee，进入 setting-> SSH key-> New SSH key，将复制的内容全部粘贴进去，然后给key起个名字即可  
  GitHub
  ![](https://z3.ax1x.com/2021/04/22/cq4NAs.png)
  Gitee
  ![](https://z3.ax1x.com/2021/04/22/cqh7Yq.png)