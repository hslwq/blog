---
title: Django基础
date: 2021-05-19 16:13:17
index_img: 
banner_img: 
tags:
- python
- django
categories:
- [python, django]
---

## 创建 Django 工程

> 如果是在 Python的虚拟环境下使用，需要先激活虚拟环境

创建Django程序

```shell
django-admin startproject mysite
```

启动服务器（Django的缺省端口为8000）

```shell
cd mysite
python manage.py runserver 127.0.0.1:8080
```

## 项目结构

```
|-- mysite
|   |-- __init__.py
|   |-- asgi.py
|   |-- settings.py
|   |-- urls.py
|   `-- wsgi.py
|-- templates
`-- manage.py
```

- mysite: 项目的容器
- manage.py: 一个实用的命令行工具，可让你以各种方式与该 Django 项目进行交互
- mysite/\_\_init\_\_.py: 一个空文件，告诉 Python 该目录是一个 Python 包
- mysite/asgi.py: 一个 ASGI 兼容的 Web 服务器的入口，以便运行你的项目
- mysite/settings.py: 该 Django 项目的设置/配置。
- mysite/urls.py: 该 Django 项目的 URL 声明; 一份由 Django 驱动的网站"目录"
- mysite/wsgi.py: 一个 WSGI 兼容的 Web 服务器的入口，以便运行你的项目
- templates: 模板目录，创建时自动生成，或者手动创建，在`settings.py`中配置

## 一个最简单的请求处理流程

urls.py

```python
from django.contrib import admin
from django.urls import path

from django.shortcuts import HttpResponse

def aaa(request):
  	"""
  	:param request: 请求的相关信息（是一个对象）
  	"""
    return HttpResponse('success')

urlpatterns = [
    # path('admin/', admin.site.urls),
  	path('aaa/', aaa()),
]
```

## 响应为html

在template目录下创建一个 login.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>
  <h1>Login</h1>
  <form action="" method="get">
    <p>用户名：<input type="text" name="username" id="username" /></p>
    <p>密码：<input type="password" name="password" id="password" /></p>
    <input type="submit" value="提交" />
  </form>
</body>
</html>
```

修改urls.py中的映射

```python
from django.urls import path

from django.shortcuts import HttpResponse, render


def login(request):
    # return HttpResponse('success')
    
    return render(request, 'login.html')

urlpatterns = [
    # path('admin/', admin.site.urls),
    path('login/', login),
]
```

## 修改配置

在`settings.py`文件中，修改项目的相关配置

### 配置模板目录

```python
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR / 'templates'] # 'templates' 就是模板的路径， BASE_DIR：项目的根路径
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
```

### 配置静态资源目录（JavaScript，CSS，Image）

```python
STATIC_URL = '/static/' # 静态资源前缀
STATICFILES_DIRS = (
    [BASE_DIR / 'static'] # 静态资源目录，
)
```

