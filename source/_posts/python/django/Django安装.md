---
title: Django 安装
date: 2021-05-11 14:13:17
index_img: 
banner_img: 
tags:
- python
- django
categories:
- [python, django]
---
# Django 安装

## 环境准备

### 安装 Python

在安装Django前，需要先安装Python，使用Django需要安装 Python3.x 以上版本

Python 的安装方法见：[菜鸟教程 | Python 环境搭建](https://www.runoob.com/python/python-install.html)

### 创建 Python 虚拟环境（venv）

> 官方介绍：[https://docs.python.org/3/tutorial/venv.html](https://docs.python.org/3/tutorial/venv.html)

1. 执行以下命令创建虚拟环境

   ```shell
   python3 -m venv venv_dir
   ```

   `venv_dir` 是虚拟环境的保存目录，如果该目录不存在，python将会自动创建

2. 启用（激活）虚拟环境

   在windows上运行：

   ```shell
   venv_dir\Script\activate.bat
   ```

   在Linux/MacOS上运行：

   ```shell
   source venv_dir/bin/activate
   ```

## Django 安装

只列出推荐方法安装，建议在 Python 的虚拟环境中安装

### 通过 pip 安装正式发布版本

```bash
pip install Django
```