---
title: Django中CSRF
date: 2021-05-11 16:13:17
index_img: 
banner_img: 
tags:
- python
- django
- CSRF
categories:
- [python, django]
---
# Django 中 FBC 和 CBV



# Django 中 CSRF 拦截器（跨站请求伪造）

> Django学习：[B站视频](https://www.bilibili.com/video/BV1ZE411j7RK?p=7)

## Django 中 CSRF 的基本使用

在 Django 中，生成项目时已经自动在配置文件 `settings.py` 加入了CSRF

```python
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware', # 该项
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]
```

上面这种相当于全局启用 CSRF，这种情况下，如果想对某个**方法**或者**类**单独关闭 CSRF，则只需在对应的方法或者类上加上一个**装饰器** `@csrf_exempt`，如下所示

```python
from django.views.decorators.csrf import csrf_exempt
@csrf_exempt # 该方法无需认证
def users(request):
    pass
```



第二种情况：全局默认关闭 CSRF

在 `settings.py` 文件中将 `django.contrib.auth.middleware.AuthenticationMiddleware` 删掉，则相当于全局关闭 CSRF

```python
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    # 'django.middleware.csrf.CsrfViewMiddleware', # 该项
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]
```

如果想对某个方法或类单独启用，同样也是只需要加上一个装饰器 `@csrf_protect`

```python
from django.views.decorators.csrf import csrf_protect
@csrf_protect # 该方法需单独认证CSRF
def users(request):
    pass
```

# Restful 规范

根据请求的 method 不同，做不同的操作

原来的定义方式：

```python
urlpatterns=[
  url(r'^add_user', views.add_user),
  url(r'^update_user', views.update_user),
  url(r'^query_users', views.query_users),
  url(r'^delete_users', views.delete_users),
]
```

```python
# 对应的view
def add_user(request):
  return HttpResponse('添加用户')

def update_user(request):
  return HttpResponse('修改用户')

def query_users(request):
  return HttpResponse('查询用户')

def delete_users(request):
  return HttpResponse('删除用户')
```

使用 restful 规范的定义方式：

```python
urlpatterns=[
  url(r'^users', views.users),
]
```

```python
def users(request):
  if request.method == 'GET':
    	return HttpResponse('查询用户')
  if request.method == 'POST':
    	return HttpResponse('添加用户')
  if request.method == 'PUT':
    	return HttpResponse('更新用户')  
  if request.method == 'DELETE':
    	return HttpResponse('删除用户')
```

