---
title: ES6语法
date: 2021-04-20 14:56:34
index_img: https://z3.ax1x.com/2021/04/26/gpFZT0.png
tags:
- js
- 前端
- ES6
categories:
- [前端, js]
---
# ES6语法
概要
- 模板字符串
    ```js
    let name = '张三';
    let str = `我是${name}`;
    console.log(str); //输出结果：我是张三
    ```
- 箭头函数
    ```js
    function(){} === ()=>{} //this指向发生了改变
    ```


## 变量声明

- #### var

- #### let

- #### const


## 函数定义

#### ES5 定义方法
```js
//es5，定义
function add(x){
    return x;
}

//es5，另一种定义方法（函数对象）
var add = function(x){
    return x;
}
```

#### ES6 定义方法
```js
//es6，定义（箭头函数）
var add = (x)=>{
    return x;
}

//es6，简写定义方法
var add = x => x;
```
以上四种定义方法，**功能相等**


## 对象

#### 定义
- 写法一
    ```js
    let person = {
        name: "张三",
        age: 20,
        fav: function(){
            console.log(this.name); //输出：张三
            console.log(this); //输入:person，this指向当前对象(person)
        }
    }
    ```
- 写法二
    ```js
    let person2 = {
        name: "张三",
        age: 20,
        fav: ()=>{
            console.log(this.name); //输出：空字符串
            console.log(this); //输入:window，this指向 定义当前对象(person2)的父级对象(上下文)
        }
    }
    ```
- 写法三：单体模式(不是单例，单体只是叫法)
    ```js
    let person3 = {
        name: "张三",
        age: 20,
        fav(){ //这种定义方式等同于 fav: function(){}
            console.log(this.name); //输出：张三
            console.log(this); //输入:person3，this指向当前对象(person3)
        }
    }
    ```

## 类

#### ES5 定义
```js
//定义类
function Person(name, age){
    this.name = name;
    this.age = age;
}

//定义方法：基于原型给对象声明方法
Person.prototype.showPersonInfo = function(){
    console.log(`我叫${this.name}，今年${this.age}岁了`)
}

let p1 = new Person("张三", 20);
p1.showPersonInfo(); //输出：我叫张三，今年20岁了
```

#### ES6 定义
```js
//定义类
class Person{
    //构造函数
    constructor(name, age){
        this.name = name;
        this.age = age;
    }
    //定义方法
    showPersonInfo(){
        console.log(`我叫${this.name}，今年${this.age}岁了`)
    }
}
```