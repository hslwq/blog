---
title: 05_Vue指令系统
date: 2021-04-21 16:09:48
index_img: https://z3.ax1x.com/2021/04/26/gpki4K.png
tags:
- vue
- 前端
categories:
- [前端, vue]
---
# 05-Vue指令系统

- ### v-text
- ### v-html
- ### v-if
- ### v-show