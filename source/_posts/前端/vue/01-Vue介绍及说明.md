---
title: 01_Vue介绍及说明
date: 2021-04-21 16:22:09
index_img: https://z3.ax1x.com/2021/04/26/gpki4K.png
tags:
- 前端
- vue
categories:
- [前端, vue]
---
# Vue介绍及说明



#### vue指令
```html
// 在vue的指令系统中，指令后一定是一个字符串，
// 里面只能填 在vue中已经定义过的data或者method
<div id="app">
  <!-- 指令后面也不能使用模板语法 双大括号{{  }}，双大括号只能用在闭合标签内，而非标签属性中 -->
  <h2 v-show="isShow"></h2>
</div>
<script>
  new Vue({
    el: '#app',
    data(){
      //data中是一个函数，该函数必须return一个对象，可以为空
      return {
        isShow: true
      }
    }
  })
</script>
```