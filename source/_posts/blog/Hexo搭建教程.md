---
title: Hexo搭建教程
date: 2021-04-21 22:31:09
index_img: https://z3.ax1x.com/2021/04/26/gpFO9U.png
tags:
- hexo
categories:
- [hexo]
---
# Hexo搭建教程
[Hexo官网](https://hexo.io/zh-cn/)
> 该教程仅针对已经同步的repo库（使用github或gitee同步）

## 环境准备
- ### 安装 Node（建议使用Node.js 14.0以上版本）
- 下载地址1：[Node.js官网](https://nodejs.org/zh-cn/)
- 下载地址2：[Node.js中文网（国内推荐）](http://nodejs.cn/)
- 下载地址3：[Node.js阿里源](https://npm.taobao.org/mirrors/node)
- ### 安装 Git
建议官网下载安装，[Git官网](https://git-scm.com/)  
安装教程建议参考：[Git教程|菜鸟教程](https://www.runoob.com/git/git-install-setup.html)

## 导入项目
从 github 或 gitee 克隆项目到本地，然后打开

## 安装 Hexo

### 全局安装
环境准备就绪后，即可使用npm安装Hexo
```shell
npm install -g hexo-cli
```

### 进阶安装和使用
对于熟悉 npm 的进阶用户，可以仅局部安装 hexo，该方法比较适合虚拟环境下使用
```shell
npm install hexo
```
安装以后，可以使用以下两种方式执行 Hexo：
  1. `npx hexo <command>`
  2. （不推荐，建议直接使用[**全局安装**](#全局安装)）将 Hexo 所在目录下的`node_modules`添加到环境变量之中即可直接使用`hexo <command>`
		```shell
		echo 'PATH="$PATH:./node_modules/.bin"' >> ~/.profile
		```

## 使用 Hexo

### 初始化
安装 Hexo 完成后，初始化环境（仅需执行一次），打开所在文件夹，执行以下命令
```shell
npm install
```
然后即可使用 Hexo

### 编译
```shell
hexo generate # 简写为 hexo g
```

### 启动服务器
```shell
hexo server #简写为 hexo s
```

## 将 Hexo 部署到 GitHub

### 安装一键部署工具 hexo-deployer-git
执行以下命令安装
```shell
npm install hexo-deployer-git --save
```

### 配置GitHub相关信息
打开Hexo配置文件`_config.yml`，翻到文件底部，修改以下信息
```yaml
deploy:
  type: git # 填入git即可
  repo: # 填入你要推送的github地址
  branch: master # 推送的分支，默认为master
  message: # 自定义提交信息，{{ now('YYYY-MM-DD HH:mm:ss') }}
```

### 部署到GitHub
以上信息全部配置完成后，执行以下命令一键部署到GitHub
```shell
hexo deploy # 简写 hexo d
```