---
title: win10搭建python的Linux开发环境
date: 2021-04-29 09:48:16
index_img: 
banner_img: 
tags:
- linux
- 虚拟环境
- vagrant
- virtualbox
- jumpserver
categories:
- [linux]
---
# win10搭建python的Linux开发环境(pycharm+vagrant+virtualbox)
> 本文参考 [安家Blog](https://anjia0532.github.io/2019/07/24/jumpserver-vagrant-virtualbox/)

{% note warning %}
由于原教程的安装在新版中会存在一点问题，所以重新整理了一份
{% endnote %}

本文主要介绍windows环境下搭建Python的Linux开发环境，使用Pycharm、Vagrant、VirtualBox搭建环境  
以Jumpserver为例

## 软件环境准备

共需要准备三个文件
- Vagrant安装包
- VirtualBox安装包
- package.box系统镜像

### win7建议安装以下版本：
1. vagrant_1.8.6.msi
2. VirtualBox-5.1.24-117012-Win.exe

### win10建议安装以下版本：

经过尝试，其实vagrant版本可以任意，最新版2.2.15也能正常运行
1. [vagrant_1.8.7.msi](https://releases.hashicorp.com/vagrant/1.8.7/) / [vagrant_1.9.7_x86_x64.msi](https://releases.hashicorp.com/vagrant/1.9.7/)
2. [VirtualBox-5.1.24-117012-Win.exe](http://download.virtualbox.org/virtualbox/5.1.24/)

### 下载package.box系统镜像
在vagrant官网下载package.box文件，也就是系统的镜像文件  
下载地址：[www.vagrantbox.es](http://www.vagrantbox.es)

## 环境安装

### Vagrant和VirtualBox安装
Vagrant和VirtualBox都是安装程序，直接下一步下一步就好了

### 把虚拟机加载到box容器中
以CentOS7为例  
打开package.box文件的存放目录，在该目录下执行命令
```shell
vagrant box add centos/7 xxx.box
```

## 下载Jumpserver并初始化虚拟机

### 下载jumpserver
从GitHub克隆Jumpserver
```shell
git clone https://github.com/jumpserver/jumpserver.git
cd jumpserver
```
在jumpserver下创建Vagrantfile，文件内容如下
```
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box_check_update = false
  config.vm.box = "centos/7"
  config.vm.hostname = "jumpserver"
  config.vm.network "private_network", ip: "172.17.8.101"
  config.vm.provider "virtualbox" do |vb|
    vb.memory = "4096"
    vb.cpus = 2
    vb.name = "jumpserver"
  end

  config.vm.synced_folder ".", "/vagrant", type: "rsync",
    rsync__verbose: true,
    rsync__exclude: ['.git*', 'node_modules*','*.log','*.box','Vagrantfile']

  config.vm.provision "shell", inline: <<-SHELL
sudo sed -e "/mirrorlist/d" -e "s/#baseurl/baseurl/g" -e "s/mirror\.centos\.org/mirrors\.tuna\.tsinghua\.edu\.cn/g" -i /etc/yum.repos.d/CentOS-Base.repo
sudo yum makecache
sudo yum install -y epel-release

sudo yum install -y python36 python36-devel python36-pip \
		 libtiff-devel libjpeg-devel libzip-devel freetype-devel \
     lcms2-devel libwebp-devel tcl-devel tk-devel sshpass \
     openldap-devel mariadb-devel mysql-devel libffi-devel \
     openssh-clients telnet openldap-clients gcc

mkdir /home/vagrant/.pip
cat << EOF | sudo tee -a /home/vagrant/.pip/pip.conf
[global]
timeout = 6000
index-url = https://mirrors.aliyun.com/pypi/simple/

[install]
use-mirrors = true
mirrors = https://mirrors.aliyun.com/pypi/simple/
trusted-host=mirrors.aliyun.com
EOF

python3.6 -m venv /home/vagrant/venv
source /home/vagrant/venv/bin/activate
echo "source /home/vagrant/venv/bin/activate" >> /home/vagrant/.bash_profile
  SHELL
end
```

### 开启虚拟机（初始化虚拟机环境）
{% note warning %}
每次执行vagrant的相关命令时，必须在对应的目录下，也就是jumpserver目录下执行
{% endnote %}

```shell
cd jumpserver
vagrant up
vagrant ssh
```
如果使用上述的Vagrantfile，则已经配置了pip的清华源和阿里源，并已经安装了python3.6

### 安装依赖
```shell
vagrant ssh #进入虚拟机
pip3 install -r /vagrant/requirements/requirements.txt
```
{% note warning %}
如果此处报权限错误，执行以下语句提升安装目录的权限等级
```shell
sudo chmod 777 -R /home/vagrant/venv
```
{% endnote %}

## 安装Jumpserver依赖（MySQL、Redis）
> 参照官方文档安装：[Jumpserver负载均衡](https://docs.jumpserver.org/zh/master/install/setup_by_lb/)

> 需要在虚拟机内进行安装

{% note warning %}
如果在安装过程中出现权限不足的情况，使用sudo权限安装即可
{% endnote %}
{% note danger %}
配置Mysql那一步，单纯使用sudo也无法安装，因为三行命令分开解析，需要先提升全局权限为root，即执行`su`命令，然后再正常执行，执行完成后，`exit`退出root权限，**否则后续安装的权限都是最高权限，部分命令会导致低权限无法访问**
{% endnote %}

### Mysql
{% note info %}
设置Repo
```
yum -y localinstall http://mirrors.ustc.edu.cn/mysql-repo/mysql57-community-release-el7.rpm
```
{% endnote %}
{% note info %}
安装Mysql
```
yum install -y mysql-community-server
```
{% endnote %}
{% note info %}
配置Mysql
```
if [ ! "$(cat /usr/bin/mysqld_pre_systemd | grep -v ^\# | grep initialize-insecure )" ]; then
    sed -i "s@--initialize @--initialize-insecure @g" /usr/bin/mysqld_pre_systemd
fi
```
{% endnote %}
{% note info %}
启动Mysql
```
systemctl enable mysqld
systemctl start mysqld
```
{% endnote %}
{% note info %}
数据库授权
```
mysql -uroot
```
```
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 2
Server version: 5.7.32 MySQL Community Server (GPL)

Copyright (c) 2000, 2020, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> create database jumpserver default charset 'utf8';
Query OK, 1 row affected (0.00 sec)

mysql> set global validate_password_policy=LOW;
Query OK, 0 rows affected (0.00 sec)

mysql> create user 'jumpserver'@'%' identified by 'weakPassword';
Query OK, 0 rows affected (0.00 sec)

mysql> grant all on jumpserver.* to 'jumpserver'@'%';
Query OK, 0 rows affected, 1 warning (0.00 sec)

mysql> flush privileges;
Query OK, 0 rows affected (0.00 sec)

mysql> exit
Bye
```
{% endnote %}
{% note info %}
配置防火墙(直接关闭)
```
systemctl disable firewalld
```
{% endnote %}

### Redis
{% note danger %}
Jumpserver官方在v2.9.2之后的版本修改了 Reids 的安装方式，改为了源码编译安装，这样会导致redis不能开机自启，且这样的步骤较为繁琐，建议采用下面的方案
{% endnote %}
{% note info %}
设置Repo
```
yum -y install epel-release https://repo.ius.io/ius-release-el7.rpm
```
{% endnote %}
{% note info %}
安装Redis
```
yum install -y redis5
```
{% endnote %}
{% note info %}
配置Redis
```
sed -i "s/bind 127.0.0.1/bind 0.0.0.0/g" /etc/redis.conf
sed -i "561i maxmemory-policy allkeys-lru" /etc/redis.conf
sed -i "481i requirepass weakPassword" /etc/redis.conf
```
{% endnote %}
{% note info %}
启动Redis
```
systemctl enable redis
systemctl start redis
```
{% endnote %}

## 配置 Pycharm

### 配置 Python Interpreter
![](https://z3.ax1x.com/2021/04/30/gANn6P.png)
![](https://z3.ax1x.com/2021/04/30/gANMm8.png)
![](https://z3.ax1x.com/2021/04/30/gANGfs.png)

### 配置 Run/Debug Configurations
![](https://z3.ax1x.com/2021/04/30/gAN07F.png)

### 配置 Django
![](https://z3.ax1x.com/2021/04/30/gANDk4.png)

### 修改配置
```shell
cd jumpserver
cp config_example.yml config.yml
```
> 也可以直接将 `config_example.yml` 文件重命名为 `config.yml` 文件

然后修改 Mysql 和 Redis 的相关配置
![](https://z3.ax1x.com/2021/04/30/gAN79A.png)

## 初始化数据库

进入对应的目录下
```shell
cd jumpserver
vagrant ssh
cd /vagrant/apps
```

执行以下命令初始化数据库
```shell
python3 manage.py migrate
```
等待执行完毕，执行完毕后，进入 jumpserver 数据库查看表是否有正常安装

## 启动项目

启动前需要先修改以下三项的值
```
SECRET_KEY: # 加密key，随机输入一个字符串
BOOTSTRAP_TOKEN: # 跟上面的一样就行
DEBUG: true # 改为true即可
```

然后启动
![](https://z3.ax1x.com/2021/04/30/gANXB8.png)

浏览器打开`http://172.17.8.101:8080`查看是否正常启动